//ANCHOR - Arrays
const numbers = [1, 2, 3, 4, 5];
const fruits = ['apple', 'banana', 'orange'];

//ANCHOR - Objects
const person = {
    name: 'John Doe',
    age: 30,
    isMarried: false
};

//ANCHOR - Maps
const map = new Map();
map.set('name', 'John Doe');
map.set('age', 30);
map.set('isMarried', false);

//ANCHOR - Sets
const set = new Set();
set.add(1);
set.add(2);
set.add(3);
set.add(2); // This will not be added, as it's already in the set.

//ANCHOR - WeakMaps
const weakMap = new WeakMap();
const key1 = { id: 1 };
const key2 = { id: 2 };
weakMap.set(key1, 'Value for key1');
weakMap.set(key2, 'Value for key2');

//ANCHOR - WeakSets
const weakSet = new WeakSet();
const obj1 = { id: 1 };
const obj2 = { id: 2 };
weakSet.add(obj1);
weakSet.add(obj2);