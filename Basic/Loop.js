//ANCHOR - For loop
for (var i = 0; i < 5; i++) {
    console.log(i);
}
// generate an array using a for loop
var arr = [];
for (var i = 0; i < 5; i++) {
  arr.push(i);
}
console.log(arr); // Output: [0, 1, 2, 3, 4]

// generate an object using a for loop
var obj = {};
for (var i = 0; i < 5; i++) {
  obj["key" + i] = i;
}
console.log(obj); // Output: { key0: 0, key1: 1, key2: 2, key3: 3, key4: 4 }

//generate an array of objects using a for loop 
var arr = [];
for (var i = 0; i < 5; i++) {
  arr.push({ key: i });
}
console.log(arr); // Output: [ { key: 0 }, { key: 1 }, { key: 2 }, { key: 3 }, { key: 4 } ]

//generate an object with array values using a for loop
var obj = {};
for (var i = 0; i < 5; i++) {
  obj["key" + i] = [i, i + 1, i + 2];
}
console.log(obj); // Output: { key0: [ 0, 1, 2 ], key1: [ 1, 2, 3 ], key2: [ 2, 3, 4 ], key3: [ 3, 4, 5 ], key4: [ 4, 5, 6 ] }


//ANCHOR - While loop
var i = 0;
while (i < 5) {
  console.log(i);
  i++;
}

//ANCHOR - do-while loop
var i = 0;
do {
  console.log(i);
  i++;
} while (i < 5);

//ANCHOR - for-in loop
var obj = {a: 1, b: 2, c: 3};
for (var prop in obj) {
  console.log(prop + ": " + obj[prop]);
}

//ANCHOR - for-of loop
var arr = [1, 2, 3];
for (var value of arr) {
  console.log(value);
}

//ANCHOR - forEach loop
var arr = [1, 2, 3];
arr.forEach(function(value) {
  console.log(value);
});

//ANCHOR - map loop
var arr = [1, 2, 3];
var newArr = arr.map(function(value) {
  return value * 2;
});
console.log(newArr); // [2, 4, 6]

//ANCHOR - filter loop
var arr = [1, 2, 3, 4, 5];
var newArr = arr.filter(function(value) {
  return value % 2 === 0;
});
console.log(newArr); // [2, 4]

//ANCHOR - reduce loop
var arr = [1, 2, 3, 4, 5];
var sum = arr.reduce(function(acc, value) {
  return acc + value;
}, 0);
console.log(sum); // 15

//ANCHOR - every loop
var arr = [1, 2, 3, 4, 5];
var result = arr.every(function(value) {
  return value > 0;
});
console.log(result); // true

//ANCHOR - some loop
var arr = [1, 2, 3, 4, 5];
var result = arr.some(function(value) {
  return value > 3;
});
console.log(result); // true
