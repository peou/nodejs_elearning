//ANCHOR - Object
let person = {
  name: "John Doe",
  age: 30,
  isMarried: true,
  hobbies: ["reading", "hiking", "coding"],
  address: {
    street: "123 Main St",
    city  : "Anytown",
    state : "CA",
    zip   : "12345",
  },
  //!SECTION: Object Method
  greet: function() {
    console.log("Hello, I am " + this.name + ".");
  },
  getAge: function() {
    return this.age;
  }
};

//ANCHOR - : Access & Modify properties
// Mark: Access
console.log(person.name); // Output: John Doe
console.log(person["age"]); // Output: 30

console.log(person.hobbies[0]); // Output: reading

console.log(person.address.street); // Output: 123 Main St
console.log(person["address"]["city"]); // Output: Anytown
// Mark: Modify
person.name           = "Jane Doe";
person.age            = 35;
person.isMarried      = false;
person.hobbies.push("swimming"); //NOTE - Push element into Array
person.address.street = "456 Elm St";

//ANCHOR - Create & Initialize Object
let person2 = new Object();
person2.name           = "Jane Doe";
person2.age            = 35;
person2.isMarried      = false;
person2.hobbies        = ["swimming"];
person2.address        = new Object();
person2.address.street = "456 Elm St";
person2.address.city   = "Othertown";
person2.address.state  = "NY";
person2.address.zip    = "67890";

//ANCHOR - Access object method
person.greet(); // Output: Hello, I am John Doe.
console.log(person.getAge()); // Output: 30