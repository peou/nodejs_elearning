function Person(name, age, isMarried, hobbies, address)
{
    this.name      = name;
    this.age       = age;
    this.isMarried = isMarried;
    this.hobbies   = hobbies;
    this.address   = address;
    this.greet = function() {
        console.log("Hello, I am " + this.name + ".");
    };
    this.getAge = function() {
        return this.age;
    };
}

let person = new Person("John Doe", 30, true, ["reading", "hiking", "coding"], {
    street: "123 Main St",
    city  : "Anytown",
    state : "CA",
    zip   : "12345"
});

person.greet(); // Output: Hello, I am John Doe.
console.log(person.getAge()); // Output: 30