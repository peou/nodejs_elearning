// NOTE : Implicit
let result = "5" - 2; 
console.log(result, "-", typeof(result)); // Output: 3 - number

result = "5" + 2; 
console.log(result, "-", typeof(result)); // Output: 52 - string

result = true + 2; 
console.log(result, "-", typeof(result)); // Output: 3 - number

// NOTE : Explicit
let input = "25";
let num = Number(input);
console.log(num, "-", typeof(num)); // Output: 25 - number

input = 25;
let str = String(input);
console.log(str, "-", typeof(str)); // Output: 25 - string

input = 0;
let bool = Boolean(input);
console.log(bool, "-", typeof(bool)); // Output: false - boolean