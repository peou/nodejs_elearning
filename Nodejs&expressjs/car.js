exports.ford = {
    branch: "Ford",
    model: "Fiesta",
};

exports.tesla = {
    branch: "Tesla",
    model: "Model 3",
};

// 1. module.exports = ford;
// 2. exports.ford = ford;