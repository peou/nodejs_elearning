// console.log("This is Nodejs Tutorial for Beginners");
// console.log("we will cover nodejs with command line");
// process.exit(0);

// 1. const car = require("./car");
// 2.const { ford } = require("./car");
// console.log(ford);

// const { tesla, ford } = require("./car");
// console.log(tesla);
// console.log(ford);

const path = require("path");


const filePath = "/Users/macbookpro/Documents/NodeJs_Elearning/Nodejs&expressjs/files/sample.txt"
/* //dirname
console.log(path.dirname(filePath));
console.log(__dirname);
// basename
console.log(path.basename(filePath));
console.log(__filename); */

// // extension
// console.log(path.extname(filePath));

// Noted: Join file
/* const sampleFile = "sample.txt";
console.log( path.join( path.dirname( filePath ) ), sampleFile ); */


const fs = require("fs");
const fsPromise = require("fs").promises;
// STEP 1:

// Reading from a file - Async
/* fs.readFile(filePath, "utf-8", (err, data) => {
    if ( err ) throw new Error (" Something went wrong!");
    console.log(data);
});
try {
    const data = fs.readFileSync(path.join(__dirname, "files", "sample.txt"), "utf-8")
    console.log(data);
} catch (err) {
    console.log(err);
} */

// STEP 2:
// Read from file

/* const filereading = async() => {
    try {
        const data = await fsPromise.readFile(filePath, {encoding: 'utf-8'});
        console.log("FS Promises: ", data);
    } catch (err) {
        console.log(err);
    }
}
filereading(); */

// STEP 3:
// Writing into file
/* const txtFile = path.join(__dirname, "files", "text.txt");
const content = "I love this nodejs tutorial series";
fs.writeFile(txtFile, content, (err) => {
    if ( err ) throw new Error (" Something went wrong!");
    console.log("Write Operation Completed Successfully!");
    fs.readFile(txtFile, "utf-8", (err, data) => {
        if ( err ) throw new Error (err);
        console.log(data);
    });
}); */

// Wriet to file and read from file and rename file
// need /files/text.txt
const txtFile = path.join(__dirname, "files", "text.txt");
const writingInFile = async () => {

    try {
        await fsPromise.writeFile(txtFile, "\n We have giving new name to the file", {
            flag: "a+",
        });
        await fs.promises.rename(
            txtFile,
            path.join(__dirname, "files", "newtxt.txt")
        );
        // await fsPromise.appendFile(txtFile, "\n this is file appender");
        const data = await fsPromise.readFile(
            path.join(__dirname, "files", "newtxt.txt")
        );
        console.log(data.toString());
    } catch (err) {
        console.log(err);
    }

}

writingInFile();