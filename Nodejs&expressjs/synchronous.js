function fun1() {
    console.log("fun 1");
}

function fun2() {
    console.log("fun 2");
    fun1();
}

function fun3() {
    console.log("fun 3");
    fun2();
}

fun3();