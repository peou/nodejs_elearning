const error = new Error("Something went wrong!");
// STEP 1:
// console.log(error.stack);
// console.log(error.message);
// throw new Error("I am error object");

const { CustomError } = require("./CustomError");
// STEP 2:
// throw new CustomError("This Custome Error Message");

// STEP 3:
// try {
//     doSomething();
// } catch (e) {
//     console.log("Error Occurred");
//     console.log(e);
// }

// function doSomething() {
//     const data = fetch("localhost:300/api")
// }

// STEP 4:
// Uncaught Exception
// process.on("uncaughException", (error) => {
//     console.log("There was an uncaught exception");
//     process.exit(1);
// });
// doSomething();

function doSomething() {
    // MARK: To test Exception
    const data = fetch("localhost:300/api")
    console.log("I am from doSomethingFunction");
    // const data = "I am from doSomethingFunction";
    return data;
}

// const promise = new Promise((resolve, reject) => {
//     if ( false ){
//         resolve(doSomething());
//     }else{
//         reject(doSomething());
//     }
// });

// promise
//     .then( (val) => {
//         console.log(val);
//     } )
//     .catch( (err) => {
//         console.log("Error Occurred");
//         console.log(err);
//     });

const someFunction = async () => {
    try {
        await doSomething();
    }catch (err) {
        throw new CustomError(err.message);
    }
};

someFunction();