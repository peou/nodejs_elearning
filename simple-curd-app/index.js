var express = require('express');
var router = express.Router();
require('dotenv').config();
const mongoose = require('mongoose');
const Product = require('./models/product.model');
const productRoute = require('./routes/product.route');
const articlesRoutes = require('./routes/article.route.js');
const usersRoutes = require('./routes/user.route.js');

const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:false})); // accept all type incomming api body Ex: form, json, param etc...

app.set('view engine', 'html');
app.use("/assets", express.static("public"));

const myport = process.env.PORT;

// route
app.get('/', (req, res) => {
    res.send(`
        <h1 style="color: red">Hello World</h1>
    `);
})
app.get('/homepage', (req, res) => {
    res.render('public/homepage.html', {
        "name" : "vannsann"
    });
})
app.use("/api/products", productRoute);
app.use('/api/articles', articlesRoutes);
app.use('/api/users', usersRoutes);
// app.use("/api/articles", )
// end route

app.listen(myport, () => {
    console.log("App is running on port 8080");
})



mongoose.connect('mongodb+srv://admin:KY0iiPW4wnXMonrm@backenddb.8qc21s4.mongodb.net/?retryWrites=true&w=majority')
.then(() => {
    console.log("Connected to DB");

})
.catch((res)=>{
    console.log(res);
    console.log("Connection failed");
})

