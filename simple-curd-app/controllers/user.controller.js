const users = require('../data/users.js');

exports.getAllUsers = (req, res) => {
    res.json(users);
};

exports.createUser = (req, res) => {
    const newUser = { ...req.body, id: users.length + 1, createdAt: new Date(), updated_at: new Date() , };
    users.push(newUser);
    res.status(201).json(newUser);
};

exports.getUserById = (req, res) => {
    const user = users.find(a => a.id === parseInt(req.params.id));
    if (!user) return res.status(404).send({ error: 'User not found' });
    res.json(user);
};

exports.updateUser = (req, res) => {
    const user = users.find(a => a.id === parseInt(req.params.id));
    if (!user) return res.status(404).send({ error: 'User not found' });
    Object.assign(user, req.body, { updated_at: new Date() });
    res.json(user);
};

exports.deleteUser = (req, res) => {
    const index = users.findIndex(a => a.id === parseInt(req.params.id));
    if (index === -1) return res.status(404).send({ error: 'User not found' });
    users.splice(index, 1);
    res.status(204).end();
};