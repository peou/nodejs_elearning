const articles = require('../data/articles.js');

exports.getAllArticles = (req, res) => {
    res.json(articles);
};

exports.createArticle = (req, res) => {
    const newArticle = { ...req.body, id: articles.length + 1, createdAt: new Date(), updatedAt: new Date() };
    articles.push(newArticle);
    res.status(201).json(newArticle);
};

exports.getArticleById = (req, res) => {
    const article = articles.find(a => a.id === parseInt(req.params.id));
    if (!article) return res.status(404).send({ error: 'Article not found' });
    res.json(article);
};

exports.updateArticle = (req, res) => {
    const article = articles.find(a => a.id === parseInt(req.params.id));
    if (!article) return res.status(404).send({ error: 'Article not found' });
    Object.assign(article, req.body, { updatedAt: new Date() });
    res.json(article);
};

exports.deleteArticle = (req, res) => {
    const index = articles.findIndex(a => a.id === parseInt(req.params.id));
    if (index === -1) return res.status(404).send({ error: 'Article not found' });
    articles.splice(index, 1);
    res.status(204).end();
};
