const express = require('express');
const path = require('path');
const multer = require('multer'); //npm install --save multer
const logger = require('morgan'); //npm install morgan
const router = express.Router();
const upload = multer({dest: "./public/uploads/"})

const app = express();

const port = 5001;
// Build-in middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// to grant access to static files http://localhost:5001/static/images/motivate_quote.jpg
app.use("/static", express.static(path.join(__dirname, "public")));

const loggerMiddleware = (req, res, next) => {
    console.log(`${new Date()} --- Request [${req.method}] [${req.url}]`);
    next();
}

app.use(loggerMiddleware);

// app.use(logger("dev"));
app.use(logger("combined"));

app.use("/api/users", router);

const fakeAuth = (req, res, next) => {
    const authStatus = true;
    if ( authStatus ) {
        console.log("User authStatus: ", authStatus);
        next();
    } else {
        res.status(401);
        throw new Error("Users is not authenticated");
    }
};

const getUsers = (req, res) => {
    res.json({
        message: "Get all user"
    });
};

const createUser = (req, res) => {
    console.log("This is the request body received from client : ", req.body);
    res.json({
        message: "Create new user"
    });
};

router.use(fakeAuth);
router.route("/").get(getUsers).post(createUser);

const errorHandler = (err, req, res, next) => {
    const statusCode = res.statusCode ? res.statusCode : 500 ;
    res.status(statusCode);
    switch ( statusCode ) {
        case 401: 
            res.json({
                title: "Unauthorization",
                message: err.message,
            });
            break;
        case 404: 
            res.json({
                title: "Not Found",
                message: err.message,
            });
            break;
        case 500: 
            res.json({
                title: "Server Error",
                message: err.message,
            });
            break;
        default:
            break;
        
    }
};

app.post("/upload", upload.single("image"), (req, res, next) => {
    console.log(req.file, req.body);
    res.send(req.file);
}, (err, req, next) => {
    res.status(400).send({err: error.message});
});

/* third party middleware
    to test it => http://localhost:5001/api/users/test */
app.all("*", (req, res) => {
    res.status(404);
    throw new Error("Route not found");
});

app.use(errorHandler);

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});