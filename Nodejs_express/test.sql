INSERT INTO
    `shop_run_back`.`order_items` (
        `order_id`,
        `product_sku`,
        `quantity`,
        `unit_price`,
        `amount`,
        `currency`,
        `pricing_item_id`,
        `pricing_grid_id`,
        `created_by`,
        `updated_by`,
        `created_at`,
        `updated_at`,
        `deleted_at`,
        `merchant_id`,
        `reference`,
        `label`,
        `price_cent`,
        `barcode`,
        `uuid`,
        `line`,
        `size`,
        `receivedAt`,
        `eligible`,
        `product_id`,
        `ext_lineitem_id`
    )
VALUES
    (
        67805,
        'MJ02-S-Green',
        1,
        51.00,
        51.00,
        'USD',
        0,
        0,
        6,
        NULL,
        '2023-09-25 04:04:20',
        '2023-09-25 04:04:20',
        NULL,
        6,
        '',
        '',
        0,
        '',
        '',
        NULL,
        NULL,
        1695600000,
        NULL,
        107464,
        NULL
    );

INSERT INTO
    `shop_run_back`.`order_items` (
        `order_id`,
        `product_sku`,
        `quantity`,
        `unit_price`,
        `amount`,
        `currency`,
        `pricing_item_id`,
        `pricing_grid_id`,
        `created_by`,
        `updated_by`,
        `created_at`,
        `updated_at`,
        `deleted_at`,
        `merchant_id`,
        `reference`,
        `label`,
        `price_cent`,
        `barcode`,
        `uuid`,
        `line`,
        `size`,
        `receivedAt`,
        `eligible`,
        `product_id`,
        `ext_lineitem_id`
    )
VALUES
    (
        67805,
        'MJ02-XS-Red',
        1,
        51.00,
        51.00,
        'USD',
        0,
        0,
        6,
        NULL,
        '2023-09-25 04:04:20',
        '2023-09-25 04:04:20',
        NULL,
        6,
        '',
        '',
        0,
        '',
        '',
        NULL,
        NULL,
        1695600000,
        NULL,
        107463,
        NULL
    );

INSERT INTO
    `shop_run_back`.`order_items` (
        `order_id`,
        `product_sku`,
        `quantity`,
        `unit_price`,
        `amount`,
        `currency`,
        `pricing_item_id`,
        `pricing_grid_id`,
        `created_by`,
        `updated_by`,
        `created_at`,
        `updated_at`,
        `deleted_at`,
        `merchant_id`,
        `reference`,
        `label`,
        `price_cent`,
        `barcode`,
        `uuid`,
        `line`,
        `size`,
        `receivedAt`,
        `eligible`,
        `product_id`,
        `ext_lineitem_id`
    )
VALUES
    (
        67805,
        'MJ02-XS-Green',
        1,
        51.00,
        51.00,
        'USD',
        0,
        0,
        6,
        NULL,
        '2023-09-25 04:04:20',
        '2023-09-25 04:04:20',
        NULL,
        6,
        '',
        '',
        0,
        '',
        '',
        NULL,
        NULL,
        1695600000,
        NULL,
        107461,
        NULL
    );